#include <SDGBioSeqDB.h>
#include <FragJoin.h>
#include <Lalign.h>
#include <FastLalign.h>
#include <FastExtAlign.h>
#include "WfaAlign.hpp"

#include "Hasher.h"

//-------------------------------------------------------------------------
// Search for diagonal of word matches with distance
void Hasher::diagSearchDist(unsigned numseqQ, Diag_map &diag_map,
                            unsigned connect_dist, unsigned kmer_size, unsigned min_frag_size,
                            std::list< RangePair >& frag, unsigned verbose) {

    unsigned count_frag = 0;
    unsigned curr_seq = 0;

    for (auto &iter_seq : diag_map) { // iter seq
        if (iter_seq.size() > 2) {
            bool extending = false;
            iter_seq.sort();
            auto iter_diag = iter_seq.begin();
            Diag prev_d = *iter_diag;
            unsigned start = 0;
            unsigned end = 0;
            long diag = 0;
            unsigned score = 0;

            while (++iter_diag != iter_seq.end()) {
                Diag curr_d = *iter_diag;
                curr_seq = curr_d.wpos.numSeq;

                if (prev_d.diag == curr_d.diag
                    && prev_d.wpos.numSeq == curr_d.wpos.numSeq
                    && (prev_d.wpos.pos + connect_dist >= curr_d.wpos.pos
                        && prev_d.wpos.pos + diag + connect_dist >= curr_d.wpos.pos + diag)
                        ) {
                    if (extending) //extending
                    {
                        end = curr_d.wpos.pos;
                        score++;
                    } else //first hit (2 kmers found at correct distance)
                    {
                        diag = prev_d.diag;
                        start = prev_d.wpos.pos;
                        end = curr_d.wpos.pos;
                        extending = true;
                        score = 1;
                    }
                } else //stop extension if distance between kmer too long
                if (extending) {
                    if (end + kmer_size - start - 1 >= min_frag_size) {
                        count_frag++;
                        frag.push_back(record_frag(start, end, diag,
                                                   score, numseqQ, curr_seq, count_frag));
                    }
                    extending = false;
                }
                prev_d = curr_d;
            } //end while
            if (extending) // Record hit at the end of the loop
            {
                if (end + kmer_size - start - 1 >= min_frag_size) {
                    count_frag++;
                    frag.push_back(record_frag(start, end, diag,
                                               score, numseqQ, curr_seq, count_frag));
                }
            }
        } //end size>2,
    } // seq loop

    if (verbose > 0) {
        std::cout << "Fragments number founds:" << count_frag << std::endl;
    }
}
//-------------------------------------------------------------------------
// Search for Alignments
void Hasher::search(const BioSeq& sequence, unsigned start, unsigned end, unsigned numseq, unsigned connect_dist,
		unsigned min_frag_size, bool repeat, std::list< RangePair >& frag, unsigned verbose)
{
    if(end-start+1<min_frag_size) return;
    clock_t clock_begin, clock_end;
    if(verbose>0){

        clock_begin = clock();
        std::cout<<"hashing query sequence #"<<numseq<<" from "<<start<<" to "<<end<<" ..."<<std::flush;
    }


    Diag_map diag_map(subject_names.size()+1);
    if(algorithm==0)
        matchKmers(sequence, numseq, start, end, repeat, diag_map);
    else if(algorithm==1)
        matchKmersHole(sequence, numseq, start, end, repeat, diag_map);
    else if(algorithm==2)
        matchKmersMinimizer(sequence, numseq, start, end, repeat, diag_map);
    else if(algorithm==3)
        matchKmersWHoleMinimizer(sequence, numseq, start, end, repeat, diag_map);

    if(verbose>0) {
        clock_end = clock();
        std::cout << " --> Time spent: " << (double) (clock_end - clock_begin) / CLOCKS_PER_SEC << " seconds"
                  << std::endl;

        clock_begin = clock();
        std::cout << "search fragments..." << std::flush;
    }

    diagSearchDist(numseq, diag_map, connect_dist, kmer_size, min_frag_size, frag, verbose-1);

	diag_map.clear();

    if(verbose>0) {
        std::cout << "ok" << std::endl;
        clock_end = clock();
        std::cout << " --> Time spent: " << (double) (clock_end - clock_begin) / CLOCKS_PER_SEC << " seconds"
                  << std::endl;
    }
}
//-------------------------------------------------------------------------
// merge found fragments
void Hasher::fragJoin(std::list< RangePair >& frag) const
{
    //separate fragments on same query and same subject sequences
    std::map<std::pair<unsigned,unsigned>, std::list<RangePair> > map_frag;
    auto it=frag.begin();
    while(it!=frag.end())
    {
        std::pair<unsigned,unsigned> key(it->getRangeQ().getNumChr(),it->getRangeS().getNumChr());
        map_frag[key].push_back(*it);
        it=frag.erase(it);
    }
    double gap_pen=pen_join;
    double dist_pen=pen_join;
    FragJoin fragJoin(dist_pen, 0, gap_pen,0);

   // merge contiguous fragments
    for (auto & item : map_frag) {
        std::list<RangePairSet> jfrag;
        fragJoin.align_all(item.second, jfrag);
        for(auto & f : jfrag){
            frag.emplace_back(f);
        }
    }
}
//-------------------------------------------------------------------------
// Merge overlapping rangePair
void Hasher::fragMerge(const std::list< RangePair >& frag, std::list< RangePair >& frag_merge)
{
    frag_merge=frag;
    frag_merge.sort(RangePair::less);

    unsigned size=(unsigned)frag_merge.size();
    if(size>=2){
        auto curr_frag_it=frag_merge.begin();
        auto next_frag_it =curr_frag_it;
        next_frag_it++;

        while(next_frag_it != frag_merge.end()) {
            if (curr_frag_it->overlapQ(*next_frag_it)) {
                //TODO chose a subject name if different
                curr_frag_it->merge(*next_frag_it);
                next_frag_it = frag_merge.erase(next_frag_it);
            } else{
                curr_frag_it++;
                next_frag_it++;
            }
        }
    }
}
//-------------------------------------------------------------------------
// Merge overlapping rangePair
unsigned Hasher::fragCoverage(const std::list< RangePair >& frag)
{
    unsigned coverage=0;
    std::list< RangePair > frag_merge;
    fragMerge(frag,frag_merge);

    for(auto & it : frag_merge)
        coverage+=it.getLength();
    return coverage;
}
//-------------------------------------------------------------------------
// Stats on rangePair lists
unsigned Hasher::fragScoreIdentityStat(const std::list< RangePair >& frag, double quantile, unsigned& coverage)
{
    coverage=0;
    if(frag.empty()){
        return 0;
    }
    std::vector<unsigned> score_list;
    std::vector<double> identity_list;
    for(const auto & curr_frag_it : frag) {
        unsigned len=curr_frag_it.getLength();
        coverage+=len;
        score_list.push_back((unsigned)curr_frag_it.getScore());
        identity_list.push_back(curr_frag_it.getIdentity());
    }
    sort(score_list.begin(), score_list.end());
    unsigned nb_frag=(unsigned)score_list.size();
    unsigned min_score=score_list.front();
    unsigned max_score=score_list.back();
    unsigned qval_score=score_list[(int)std::floor((double)score_list.size() * quantile)];
    std::cout << "Frag number=" << nb_frag << " / "
              << "min score=" << min_score << " / "
              << "max score=" << max_score << " / "
              << "quantile score (" << quantile << ")=" << qval_score
             <<std::endl;
    sort(identity_list.begin(), identity_list.end());
    unsigned min_identity=identity_list.front();
    unsigned max_identity=identity_list.back();
    unsigned qval_identity=identity_list[(int)std::floor((double)identity_list.size() * quantile)];
    std::cout << "     min identity=" << min_identity << " / "
              << "max identity=" << max_identity << " / "
              << "quantile identity (" << quantile << ")=" << qval_identity
              <<std::endl;
    return qval_score;
}
//-------------------------------------------------------------------------
unsigned Hasher::fragLengthStat(const std::list< RangePair >& frag, double quantile)
{
    if(frag.empty()){
        return 0;
    }
    std::vector<unsigned> length_list;
    for(const auto & curr_frag_it : frag) {
        length_list.push_back(curr_frag_it.getLength());
    }
    sort(length_list.begin(), length_list.end());
    unsigned nb_frag=(unsigned)length_list.size();
    unsigned min_score=length_list.front();
    unsigned max_score=length_list.back();
    unsigned qval=length_list[(int)std::floor((double)length_list.size() * quantile)];
    std::cout << "Frag number=" << nb_frag << " / "
              << "min length=" << min_score << " / "
              << "max length=" << max_score << " / "
              <<"quantile length ("<<quantile<<")="<<qval
              <<std::endl;
    return qval;
}
//-------------------------------------------------------------------------
// Filter length on rangePair lists
void Hasher::fragLenFilter(std::list< RangePair >& frag, unsigned min_len)
{
    std::cout<<"--Filter fragments length <"<<min_len<<" ... "<<std::flush;
    auto frag_it=frag.begin();
    while (frag_it != frag.end()) {
        if (frag_it->getLength() < min_len) {
            frag_it = frag.erase(frag_it);
        } else { frag_it++; }
    }
    std::cout<<"done !"<<std::endl;
}
//-------------------------------------------------------------------------
// Filter score on rangePair lists
void Hasher::fragScoreIdentityFilter(std::list<RangePair> &frag, unsigned min_score, unsigned min_identity)
{
    std::cout<<"--Filter fragments score <"<<min_score<<" or identity <"<<min_identity<<" ... "<<std::flush;
    auto frag_it=frag.begin();
    while(frag_it != frag.end()) {
        if(frag_it->getScore()<min_score || frag_it->getIdentity()<min_identity){
            frag_it = frag.erase(frag_it);
        }else{frag_it++;}
    }
    std::cout<<"done !"<<std::endl;
}
//-------------------------------------------------------------------------
// Set rangePair score and identity
void Hasher::fragSeqAlign(std::list< RangePair >& frag,
                          const SDGString& fasta_queryfilename, const SDGString& fasta_subjectfilename,
                          bool reverse, unsigned verbose)
{
    FastaIstream query_in(fasta_queryfilename);
    if (!query_in) {
        std::cerr << "file:" << fasta_queryfilename << " does not exist!" << std::endl;
    }

    std::vector<BioSeq> subject_db;
    FastaIstream subject_in(fasta_subjectfilename);
    if (!subject_in) {
        std::cerr << "file:" << fasta_queryfilename << " does not exist!" << std::endl;
    }
    while (subject_in) {
        BioSeq seq;
        if (subject_in) {
            subject_in >> seq;
            subject_db.push_back(seq);
        }
    }

    unsigned numseq=0;
    while (query_in) {
        BioSeq seq;
        if (query_in)
            query_in >> seq;
        if(reverse){
            seq=seq.reverse();
        }
        numseq++;
        if(verbose>0) std::cout << seq.header << " len:" << seq.size() << " read!" << std::endl;
        for (auto & curr_frag_it : frag) {
            if (curr_frag_it.getRangeQ().getNumChr() == numseq) {
                if(verbose>0) curr_frag_it.view();
                // RangePair on the current query sequence
                BioSeq qseq;
                qseq = seq.subseq((unsigned)curr_frag_it.getRangeQ().getMin()-1,
                                  (unsigned)curr_frag_it.getRangeQ().getLength());
                if (!curr_frag_it.getRangeQ().isPlusStrand()) {
                    qseq = qseq.complement();
                }
                unsigned qlen=(unsigned)qseq.size();

                if(verbose>0) std::cout << "query:  " << qseq << "-" << qlen << std::endl;
                // Get subject sequence

                BioSeq sseq = subject_db[curr_frag_it.getRangeS().getNumChr()-1];

                BioSeq fragsseq;
                fragsseq = sseq.subseq((unsigned)curr_frag_it.getRangeS().getMin()-1,
                                       (unsigned)curr_frag_it.getRangeS().getLength());
                if (!curr_frag_it.getRangeS().isPlusStrand()) {
                    fragsseq = fragsseq.complement();
                }
                unsigned slen=(unsigned)fragsseq.size();
                if(verbose>0) std::cout << "subject:" << fragsseq << "-" << slen << std::endl;
                unsigned count=0;
                int score=0;

                unsigned len_align=std::min(qlen,slen);
                for(unsigned i=0;i<len_align;i++)
                {
                    if(qseq[i]==fragsseq[i]){
                        count++;
                        score+=5;
                    }else{score+=-4;}
                }
                if(score<0){score=0;}
                curr_frag_it.setIdentity(((double)count)/len_align*100);
                curr_frag_it.setScore(score);
                if(verbose>0) std::cout << "Score = " << curr_frag_it.getScore()<<" identity = " << curr_frag_it.getIdentity()<< std::endl;
            }
        }
    }
}
//-------------------------------------------------------------------------
// SW Align rangePair
void Hasher::fragSeqSWAlign(std::list< RangePair >& frag, unsigned ext_len,
                            const SDGString& fasta_queryfilename, const SDGString& fasta_subjectfilename,
                            bool reverse, unsigned verbose)
{
/*
    wavefront_aligner_attr_t attributes = wavefront_aligner_attr_default;
    attributes.distance_metric = gap_affine;
    attributes.affine_penalties.match = 0;
    attributes.affine_penalties.mismatch = 4;
    attributes.affine_penalties.gap_opening = 6;
    attributes.affine_penalties.gap_extension = 2;
    
    attributes.alignment_form.span = alignment_endsfree;
    attributes.alignment_form.pattern_begin_free = 0;
    attributes.alignment_form.pattern_end_free = 0;
    attributes.alignment_form.text_begin_free = 0;
    attributes.alignment_form.text_end_free = 0;
    // Initialize Wavefront Aligner
    wavefront_aligner_t* const wf_aligner = wavefront_aligner_new(&attributes);
    
    FastaIstream query_in(fasta_queryfilename);
    if (!query_in) {
        std::cerr << "file:" << fasta_queryfilename << " does not exist!" << std::endl;
    }

    std::vector<BioSeq> subject_db;
    FastaIstream subject_in(fasta_subjectfilename);
    if (!subject_in) {
        std::cerr << "file:" << fasta_queryfilename << " does not exist!" << std::endl;
    }
    while (subject_in) {
        BioSeq seq;
        if (subject_in) {
            subject_in >> seq;
            subject_db.push_back(seq);
        }
    }

    unsigned numseq=0;
    unsigned nb_align=0;
    while (query_in) {
        BioSeq qseq;
        if (query_in)
            query_in >> qseq;
        if(reverse){
            qseq=qseq.reverse();
        }
        numseq++;
        if(verbose>0) std::cout << qseq.header << " len:" << qseq.size() << " read!" << std::endl;
        for (auto & curr_frag_it : frag) {
            if (curr_frag_it.getRangeQ().getNumChr() == numseq) {
                if(verbose>0) {
                    curr_frag_it.view(); }
                // RangePair on the current query sequence
                BioSeq fragqseq;
                unsigned qstart,qlen;

                if((unsigned)curr_frag_it.getRangeQ().getMin()-1<ext_len)
                    qstart=0;
                else
                    qstart=(unsigned)curr_frag_it.getRangeQ().getMin()-1-ext_len;

                if(curr_frag_it.getRangeQ().getLength()+2*ext_len > qseq.size()){
                    qlen= (unsigned)qseq.size() - qstart;
                }
                else{
                    qlen=(unsigned)curr_frag_it.getRangeQ().getLength()+2*ext_len;
                }

                fragqseq = qseq.subseq(qstart, qlen);

                if (!curr_frag_it.getRangeQ().isPlusStrand()) {
                    fragqseq = fragqseq.complement();
                }
                qlen=(unsigned)fragqseq.size();

                // Get subject sequence

                BioSeq sseq = subject_db[curr_frag_it.getRangeS().getNumChr()-1];

                BioSeq fragsseq;
                unsigned sstart,slen;

                if(curr_frag_it.getRangeS().getMin()-1<ext_len)
                    sstart=0;
                else
                    sstart=(unsigned)curr_frag_it.getRangeS().getMin()-1-ext_len;

                if(curr_frag_it.getRangeS().getLength()+2*ext_len > sseq.size()){
                    slen=(unsigned)sseq.size()-sstart;
                }
                else{
                    slen=(unsigned)curr_frag_it.getRangeS().getLength()+2*ext_len;
                }


                fragsseq = sseq.subseq(sstart,slen);
                if (!curr_frag_it.getRangeS().isPlusStrand()) {
                    fragsseq = fragsseq.complement();
                }

                slen=(unsigned)fragsseq.size();

                if(++nb_align % 100 ==0) std::cout<<std::endl;
                std::cout<<"."<<std::flush;
                if(verbose>0){
                    std::cout<<"aligning sequence of length : "<<qlen<<" ... "<<std::flush;
                    
                    std::cout<<"\nQuery/Pattern:"<<fragqseq<<std::endl;
                    std::cout<<" Subject/Text:"<<fragsseq<<std::endl;
                }

                const char *pattern=fragqseq.c_str();
                const char *text=fragsseq.c_str();

                // Align
                wavefront_align(wf_aligner,pattern,strlen(pattern),text,strlen(text));
                if(verbose>0){
                    fprintf(stderr,"WFA-Alignment returns score %d\n",wf_aligner->cigar->score);
                    // Display alignment
                    fprintf(stderr,"  PATTERN  %s\n",pattern);
                    fprintf(stderr,"  TEXT     %s\n",text);
                    fprintf(stderr,"  SCORE (RE)COMPUTED %d\n",
                            cigar_score_gap_affine(wf_aligner->cigar,&attributes.affine_penalties));
                    cigar_print_pretty(stderr,
                                       pattern,strlen(pattern),text,strlen(text),
                                       wf_aligner->cigar,wf_aligner->mm_allocator);
                    
                }
                // Count mismatches, deletions, and insertions
                int i, mat=0, misms=0, ins=0, del=0;
                cigar_t* const cigar = wf_aligner->cigar;
                for (i=cigar->begin_offset;i<cigar->end_offset;++i) {
                  switch (cigar->operations[i]) {
                    case 'M': mat++;break;
                      case 'X': ++misms; break;
                      case 'D': ++del; break;
                      case 'I': ++ins; break;
                    }
                }

                unsigned start_p=0;
                for (i=cigar->begin_offset;i<cigar->end_offset;++i) {
                    if(cigar->operations[i]=='I') start_p++;
                    else break;
                }
                unsigned end_p=qlen;
                for (i=cigar->end_offset;i>cigar->begin_offset;--i) {
                    if(cigar->operations[i]=='I') end_p--;
                    else break;
                }
                if(verbose>0) std::cout<<"offset PATTERN begin:"<<start_p<<" end:"<<end_p<<std::endl;

                unsigned start_t=0;
                for (i=cigar->begin_offset;i<cigar->end_offset;++i) {
                    if(cigar->operations[i]=='D') start_t++;
                    else break;
                }
                unsigned end_t=slen;
                for (i=cigar->end_offset;i>cigar->begin_offset;--i) {
                    if(cigar->operations[i]=='D') end_t--;
                    else break;
                }
                if(verbose>0) std::cout<<"offset TEXT begin:"<<start_t<<" end:"<<end_t<<std::endl;
                unsigned alen=cigar->end_offset-cigar->begin_offset;
                float identity=float(mat)/(mat+misms)*100;
                int score=cigar->score;
                
                if(verbose>0) std::cout<<"identity:"<<identity<<" score:"<<score<<" alignment length:"<<alen<<std::endl;

                curr_frag_it.setIdentity(identity);
                curr_frag_it.setScore(score);
                curr_frag_it.setLength(alen);

                if(verbose>0) {
                    std::cout<<" done !"<<std::endl;
                    std::cout << "----result---->";
                    curr_frag_it.view(); }
                if(verbose>0) std::cout << "Score = " << curr_frag_it.getScore()<<" identity = " << curr_frag_it.getIdentity()<< std::endl;
            }
        }
    }
    // Free
    wavefront_aligner_delete(wf_aligner);
*/
}
//-------------------------------------------------------------------------
// Extend boundaries by alignment
void Hasher::fragSeq1ExtAlign(RangePair& frag, unsigned ext_len,
                            BioSeq& qseq, BioSeq& sseq,
                            bool reverse, unsigned verbose){

    WfaAlign align(0,1,2,WFAligner::Alignment,WFAligner::MemoryHigh);
    int xdrop=5;
    int upstreamEndQ=0, upstreamEndS=0,downstreamEndQ=0, downstreamEndS=0;
    if ( frag.getRangeQ().isPlusStrand() 
            && frag.getRangeS().isPlusStrand() ) 
        {
            //Extend on + strand
            if(verbose>0) std::cout<<"+ strand +"<<std::endl;
            //Downstream extension
            if(verbose>0) std::cout<<"-downstream extension"<<std::endl;
            
            align.Xdrop(qseq,sseq,xdrop,
                        frag.getRangeQ().getMax(),
                        frag.getRangeS().getMax(),
                        ext_len,true,true,verbose);
            downstreamEndQ=align.getEndQ();
            downstreamEndS=align.getEndS();

            if(verbose>0) std::cout<<"identity:"<<align.getIdentity()<<" score:"<<align.getScore()<<" alignment length:"<<align.getALen()<<std::endl;


            //Upstream extension
            if(verbose>0) std::cout<<"-upstream extension"<<std::endl;
            align.Xdrop(qseq,sseq,xdrop,
                        frag.getRangeQ().getMin(),
                        frag.getRangeS().getMin(),
                        ext_len,false,true,verbose);
            upstreamEndQ=align.getEndQ()-1;
            upstreamEndS=align.getEndS()-1;

            frag.getRangeQ().setEnd(frag.getRangeQ().getEnd()+downstreamEndQ);
            frag.getRangeS().setEnd(frag.getRangeS().getEnd()+downstreamEndS);
            frag.getRangeQ().setStart(frag.getRangeQ().getStart()-upstreamEndQ);
            frag.getRangeS().setStart(frag.getRangeS().getStart()-upstreamEndS);
            if(verbose>0) std::cout<<"identity:"<<align.getIdentity()<<" score:"<<align.getScore()<<" alignment length:"<<align.getALen()<<std::endl;
            

        }else{
            //Extend on - strand
            if(verbose>0) std::cout<<"- strand +"<<std::endl;
            //Downstream extension
            if(verbose>0) std::cout<<"-downstream extension"<<std::endl;
            align.Xdrop(qseq,sseq,xdrop,
                        frag.getRangeQ().getMax(),
                        frag.getRangeS().getMax(),
                        ext_len,true,false,verbose);
            downstreamEndQ=align.getEndQ();
            downstreamEndS=align.getEndS();
            if(verbose>0) std::cout<<"identity:"<<align.getIdentity()<<" score:"<<align.getScore()<<" alignment length:"<<align.getALen()<<std::endl;
            

            //Upstream extension
            if(verbose>0) std::cout<<"-upstream extension"<<std::endl;
            align.Xdrop(qseq,sseq,xdrop,
                        frag.getRangeQ().getMin(),
                        frag.getRangeS().getMin(),
                        ext_len,false,false, verbose);
            upstreamEndQ=align.getEndQ()-1;
            upstreamEndS=align.getEndS()-1;

            frag.getRangeQ().setStart(frag.getRangeQ().getStart()+downstreamEndQ);
            frag.getRangeS().setEnd(frag.getRangeS().getEnd()+downstreamEndS);
            frag.getRangeQ().setEnd(frag.getRangeQ().getEnd()-upstreamEndQ);
            frag.getRangeS().setStart(frag.getRangeS().getStart()-upstreamEndS);

            if(verbose>0) std::cout<<"identity:"<<align.getIdentity()<<" score:"<<align.getScore()<<" alignment length:"<<align.getALen()<<std::endl;
        }                                
}
//-------------------------------------------------------------------------
// Extend boundaries by alignment
void Hasher::fragSeqExtAlign(std::list< RangePair >& frag, unsigned ext_len,
                            const SDGString& fasta_queryfilename, const SDGString& fasta_subjectfilename,
                            bool reverse, unsigned verbose)
{
    
    
    FastaIstream query_in(fasta_queryfilename);
    if (!query_in) {
        std::cerr << "file:" << fasta_queryfilename << " does not exist!" << std::endl;
    }

    std::vector<BioSeq> subject_db;
    FastaIstream subject_in(fasta_subjectfilename);
    if (!subject_in) {
        std::cerr << "file:" << fasta_queryfilename << " does not exist!" << std::endl;
    }
    while (subject_in) {
        BioSeq seq;
        if (subject_in) {
            subject_in >> seq;
            subject_db.push_back(seq);
        }
    }

    bool denovo=false;
    if(fasta_queryfilename==fasta_subjectfilename) denovo=true;

    
    unsigned start_ext_len, end_ext_len;
    unsigned numseq=0;
    unsigned nb_align=0;
    while (query_in) {
        BioSeq qseq;
        if (query_in)
            query_in >> qseq;
        if(reverse){
            qseq=qseq.reverse();
        }
        numseq++;
        if(verbose>0) std::cout << qseq.header << " len:" << qseq.size() << " read!" << std::endl;
        for (auto & curr_frag_it : frag) {
            if (curr_frag_it.getRangeQ().getNumChr() == numseq) {
                
                if(verbose>0) {
                    std::cout << "----ori---->";
                    curr_frag_it.view();}
                
                // Get subject sequence
                BioSeq sseq = subject_db[curr_frag_it.getRangeS().getNumChr()-1];

                if(verbose>0) std::cout<<"aligning sequence ... "<<std::flush;

                //do someting for denovo mode ?
                
                fragSeq1ExtAlign(curr_frag_it, ext_len,qseq, sseq,reverse,verbose);
                
                // RangePair on the current query sequence
                
                if(++nb_align % 100 ==0) std::cout<<std::endl;
                std::cout<<"."<<std::flush;
             
                if(verbose>0) {
                    std::cout << "----result---->";
                    curr_frag_it.view(); }
                if(verbose>0) std::cout << "Score = " << curr_frag_it.getScore()<<" identity = " << curr_frag_it.getIdentity()<< std::endl;
            }
        }
    }

}
//-------------------------------------------------------------------------
// Write a rangePair lists
void Hasher::fragAlignWrite(std::list< RangePair >& frag, const SDGString& qfilename, const SDGString& sfilename, std::ostream& out)
{
    std::vector<std::string> num2nameQ,num2nameS;

    std::ifstream fileQ(qfilename);
    std::string line;
    if (fileQ.is_open()) {
        while (std::getline(fileQ, line)) {
            if (line[0] == '>') {
                line.erase(0, 1);
                line.erase(line.find_last_not_of("\t\n\v\f\r ") + 1);
                num2nameQ.push_back(line);
            }
        }
    }
    fileQ.close();

    std::ifstream fileS(sfilename);
    if (fileS.is_open()) {
        while (std::getline(fileS, line)) {
            if (line[0] == '>') {
                line.erase(0, 1);
                line.erase(line.find_last_not_of("\t\n\v\f\r ") + 1);
                num2nameS.push_back(line);
            }
        }
    }
    fileS.close();

    for(auto & curr_frag_it : frag) {
        if(curr_frag_it.getNumQuery()>(long)num2nameQ.size()){
            std::cerr<<"Error query sequence "<<curr_frag_it.getRangeQ().getNameSeq()<<" number "<<curr_frag_it.getNumQuery()<<" doesn't exist!"<<std::endl;
            exit(EXIT_FAILURE);
        }
        SDGString qname=num2nameQ[curr_frag_it.getNumQuery()-1];

        if(curr_frag_it.getNumSubject()>(long)num2nameS.size()){
            std::cerr<<"Error subject sequence "<<curr_frag_it.getRangeS().getNameSeq()<<" number "<<curr_frag_it.getNumSubject()<<" doesn't exist!"<<std::endl;
            exit(EXIT_FAILURE);
        }
        SDGString sname=num2nameS[curr_frag_it.getNumSubject()-1];
        curr_frag_it.setQSName(qname,sname);
        curr_frag_it.write(out);
    }
}
//-------------------------------------------------------------------------
// Write a rangePair sequences
void Hasher::fragSeqWrite(const std::list< RangePair >& frag, const SDGString& fasta_filename, FastaOstream& out)
{
    FastaIstream in(fasta_filename);
    if (!in) {
        std::cerr << "file:" << fasta_filename << " does not exist!" << std::endl;
    }
    unsigned numseq=0;
    while (in) {
        BioSeq seq;
        if (in)
            in >> seq;
        numseq++;
        std::cout << seq.header << " len:" << seq.size() << " read!" << std::endl;
        for (const auto & curr_frag : frag) {
            if (curr_frag.getRangeQ().getNumChr() == numseq) {
                BioSeq sseq;
                if (curr_frag.getRangeQ().isPlusStrand()) {
                    sseq = seq.subseq(curr_frag.getRangeQ().getStart(), curr_frag.getRangeQ().getEnd() - curr_frag.getRangeQ().getStart() + 1);
                } else {
                    sseq = seq.subseq(curr_frag.getRangeQ().getEnd(), curr_frag.getRangeQ().getStart() - curr_frag.getRangeQ().getEnd() + 1);
                    sseq = sseq.complement();
                }
                std::istringstream subject_name(curr_frag.getRangeS().getNameSeq());
                std::string prefix_name;
                subject_name>>prefix_name;
                std::ostringstream name;
                name << prefix_name << " " << seq.header << ":"
                     << curr_frag.getRangeQ().getStart() << ".."
                     << curr_frag.getRangeQ().getEnd();
                sseq.header=name.str();
                out << sseq;
            }
        }
    }
}
//-------------------------------------------------------------------------
// Write a rangePair sequences
void Hasher::fragMergeSeqWrite(const std::list< RangePair >& frag, const SDGString& fasta_filename, FastaOstream& out)
{
    std::list< RangePair > frag_merge;
    fragMerge(frag,frag_merge);
    fragSeqWrite(frag_merge,fasta_filename,out);
}
