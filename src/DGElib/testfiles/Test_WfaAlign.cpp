//
//  Test_WfaAlign.cpp
//  test_DGE
//
//  Created by Hadi Quesneville on 08/07/2023.
//
#include "Test_WfaAlign.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(Test_WfaAlign);
//------------------------------------------------------------------------------------------------------------
void Test_WfaAlign::test_XdropAlign( void )
{
  
    BioSeq te_insert=seq1.subseq(0,50);
    std::ostringstream ostr;
    ostr<<te_insert.c_str()<<seq2.subseq(100,50).c_str();
    BioSeq genome=ostr.str();

    int verbose=0;

    WfaAlign align(0,1,2,WFAligner::Alignment,WFAligner::MemoryHigh);

    //WfaAlign align(4,6,2,WFAligner::Alignment,WFAligner::MemoryHigh);

    int xdrop=5;
    align.XdropAlign(seq1,genome,xdrop,verbose);

    CPPUNIT_ASSERT_EQUAL(52,align.getALen());
}
//------------------------------------------------------------------------------------------------------------
void Test_WfaAlign::test_extractSeq( void )
{
    std::ostringstream ostr1;
    ostr1<<"ATATTTATTTTAGCGTTTACGCTATGTGTTGCGTATTGCTAATCGCTATG";
    ostr1<<"TTACGCTATGTGTTATTTTTAGCGTTATTGCTAGCGTTTGCGATATTTAT";
    ostr1<<"ATATTTCGCGCTATGTGTTGCGATAGCGTTTATTATACCTATATCGCTAT";
    BioSeq seq=BioSeq(ostr1.str());

    WfaAlign align;

    //test downstream
    BioSeq obs_seq_downstream=align.extractSeq(seq,130,50);

    std::ostringstream ostr2;
    ostr2<<"TATTATACCTATATCGCTAT";
    BioSeq exp_seq_downstream=BioSeq(ostr2.str());

    CPPUNIT_ASSERT_EQUAL(exp_seq_downstream,obs_seq_downstream);

    //test upstream
    BioSeq obs_seq_upstream=align.extractSeq(seq,20,50,false);

    std::ostringstream ostr3;
    ostr3<<"ATATTTATTTTAGCGTTTAC";
    BioSeq exp_seq_upstream=BioSeq(ostr3.str());

    CPPUNIT_ASSERT_EQUAL(exp_seq_upstream,obs_seq_upstream);
}
//------------------------------------------------------------------------------------------------------------
void Test_WfaAlign::test_XdropDownstreamIsPlusStrand( void )
{
    
    BioSeq te_insert=seq1.subseq(0,50);
    std::ostringstream ostr;
    ostr<<te_insert.c_str()<<seq2.subseq(100,50).c_str();
    BioSeq genome=ostr.str();

    int verbose=0;
    
    WfaAlign align(0,1,2,WFAligner::Alignment,WFAligner::MemoryHigh);

    int xdrop=5;
    align.Xdrop(genome,seq1,xdrop,20,20,50,true,true,verbose);

    CPPUNIT_ASSERT_EQUAL(32,align.getALen());
}
//------------------------------------------------------------------------------------------------------------
void Test_WfaAlign::test_XdropUpstreamIsPlusStrand( void )
{
    
    BioSeq te_insert=seq1.subseq(20,50);
    std::ostringstream ostr;
    ostr<<seq2.subseq(100,20).c_str()<<te_insert.c_str();
    BioSeq genome=ostr.str();
    
    int verbose=0;
    
    WfaAlign align(0,1,2,WFAligner::Alignment,WFAligner::MemoryHigh);

    int xdrop=5;
    align.Xdrop(genome,seq1,xdrop,50,50,50,false,true,verbose);

    CPPUNIT_ASSERT_EQUAL(30,align.getALen());
}
//------------------------------------------------------------------------------------------------------------
void Test_WfaAlign::test_XdropDownstreamIsNotPlusStrand( void )
{
    
    BioSeq te_insert=seq1.complement().subseq(20,50);
    std::ostringstream ostr;
    ostr<<te_insert.c_str()<<seq2.subseq(100,50).c_str();
    BioSeq genome=ostr.str();

    int verbose=0;
    
    WfaAlign align(0,1,2,WFAligner::Alignment,WFAligner::MemoryHigh);

    int xdrop=5;
    align.Xdrop(genome, seq1, xdrop,20,40,50,true,false,verbose);

    CPPUNIT_ASSERT_EQUAL(30,align.getALen());
}
//------------------------------------------------------------------------------------------------------------
void Test_WfaAlign::test_XdropUpstreamIsNotPlusStrand( void )
{
    
    BioSeq te_insert=seq1.complement().subseq(20,50);
    std::ostringstream ostr;
    ostr<<seq2.subseq(100,20).c_str()<<te_insert.c_str();
    BioSeq genome=ostr.str();

    int verbose=0;
    
    WfaAlign align(0,1,2,WFAligner::Alignment,WFAligner::MemoryHigh);

    int xdrop=5;
    align.Xdrop(genome,seq1,xdrop,50,50,50,false,false,verbose);

   CPPUNIT_ASSERT_EQUAL(30,align.getALen());
}
