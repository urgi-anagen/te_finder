//
//  Test_WfaAlign.hpp
//  test_DGE
//
//  Created by Hadi Quesneville on 08/07/2023.
//

#ifndef Test_WfaAlign_hpp
#define Test_WfaAlign_hpp
#include <cppunit/extensions/HelperMacros.h>
#include <cstdio>

#include "WfaAlign.hpp"

class Test_WfaAlign : public CppUnit::TestFixture{
    CPPUNIT_TEST_SUITE(Test_WfaAlign);

    CPPUNIT_TEST( test_XdropAlign );
    CPPUNIT_TEST( test_XdropDownstreamIsPlusStrand );
    CPPUNIT_TEST( test_XdropUpstreamIsPlusStrand );
    CPPUNIT_TEST( test_XdropDownstreamIsNotPlusStrand );
    CPPUNIT_TEST( test_XdropUpstreamIsNotPlusStrand );
    CPPUNIT_TEST( test_extractSeq );

    CPPUNIT_TEST_SUITE_END();

    BioSeq seq1, seq2;

public:

    Test_WfaAlign(void) {setUp();}

    void setUp()
    {
            std::ostringstream ostr1;
            ostr1<<"ATATTTATTTTAGCGTTTACGCTATGTGTTGCGTATTGCTAATCGCTATG";
            ostr1<<"TTACGCTATGTGTTATTTTTAGCGTTATTGCTAGCGTTTGCGATATTTAT";
            ostr1<<"ATATTTCGCGCTATGTGTTGCGATAGCGTTTATTATACCTATATCGCTAT";
            seq1=BioSeq(ostr1.str());

            std::ostringstream ostr2;
            ostr2<<"ATATTTCGCGCTATGTGTTGCGATAGCGTTTATTATACCTATATCGCTAT";
            ostr2<<"TTACGCTATGTGTTATTTTTAGCGTTATTGCTAGCGTTTGCGATATTTAT";
            ostr2<<"AATCGCTATGAATTTCAGTCTGGCTACTTTCACGTACGATGACAGACTCT";
            ostr2<<"AAACTTTCACGTACGTGACAGTCTCT";
            seq2=BioSeq(ostr2.str());
            seq2=seq2.reverse();
    }
    void tearDown()
    {
    }

protected:
    void test_XdropAlign(void);
    void test_XdropDownstreamIsPlusStrand(void);
    void test_XdropUpstreamIsPlusStrand(void);
    void test_XdropDownstreamIsNotPlusStrand(void);
    void test_XdropUpstreamIsNotPlusStrand(void);
    void test_extractSeq( void );
};


#endif /* Test_WfaAlign_hpp */

