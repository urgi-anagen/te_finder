//
//  WfaAlign.cpp
//  test_DGE
//
//  Created by Hadi Quesneville on 08/07/2023.
//
#include <stdio.h>
#include "WfaAlign.hpp"


//------------------------------------------------------------------------------
void WfaAlign::XdropAlign(const BioSeq& query_seq, const BioSeq& subject_seq, int xdrop, unsigned verbose){
    std::string pattern=subject_seq;
    std::string text=query_seq;
    std::string cigar;

    // Align
    setHeuristicXDrop(100,100);
    if(pattern.size() >= gap && text.size()>=gap){
        alignEndsFree(pattern, 0,pattern.size(), text, 0, text.size());
        cigar = getAlignment();
    }

    if(verbose>0){
        std::cout << "\nWFA-Alignment returns score : " << getAlignmentScore() << std::endl;
        
        // Print CIGAR
        
        std::cout << "PATTERN: " << pattern  << std::endl;
        std::cout << "TEXT:    " << text  << std::endl;
        std::cout << "CIGAR:   " << cigar  << std::endl;
    }

    // Count mismatches, deletions, and insertions
    alen=mat=misms=ins=del=0;
    int max_sc=0;
    int max_end_t=0;
    int max_end_p=0;
    int max_alen=0;
    int max_mat=0;
    int max_misms=0;
    int sc=0;
    int align_len=0;
    end_t=0;
    end_p=0;
    for (std::string::iterator it=cigar.begin();it!=cigar.end();it++) {
        switch (*it) {
            case 'M': {mat++; end_t++; end_p++; sc+=match; break;}
            case 'X': {misms++; end_t++; end_p++; sc-=mismatch; break;}
            case 'D': {del++; end_p++; sc-=gap;break;}
            case 'I': {ins++; end_t++; sc-=gap;break;}
        }
        align_len++;
        if(sc>=max_sc || it==cigar.begin()){
            max_sc=sc;
            max_end_p=end_p;
            max_end_t=end_t;
            max_alen=align_len;
            max_mat=mat;
            max_misms=misms;
        }
        if(max_sc-sc>=xdrop){
            break;
        }
    }
    end_p=max_end_p;
    end_t=max_end_t;
    alen=max_alen;
    mat=max_mat;
    misms=max_misms;

    identity=float(mat)/(mat+misms)*100;
    score=max_sc;
    if(verbose>0){
        viewAlignment(pattern,text);
    }
}
//------------------------------------------------------------------------------
void WfaAlign::viewAlignment(const BioSeq& query_seq, const BioSeq& subject_seq, unsigned verbose)
{
    if(query_seq.size()<gap || subject_seq.size()<gap) return;
    std::string cigar = getAlignment();
    int q=0;
    int s=0;
    int c=0;
    
    std::ostringstream up, mid, down, scale;
    
    for (std::string::iterator it=cigar.begin();it!=cigar.end();++it) {
        if(++c%10==0){scale<<":";}
        else{ 
            if(c%5==0)scale<<"+";
            else scale<<".";
            };
        switch (*it) {
            case 'M': {
                up<<query_seq[q++];
                mid<<"|";
                down<<subject_seq[s++];
                break;
            }
            case 'X': {
                up<<query_seq[q++];
                mid<<" ";
                down<<subject_seq[s++];
                break;
            }
            case 'D': {
                up<<query_seq[q++];
                mid<<" ";
                down<<"-";
                break;
            }
            case 'I': {
                up<<"-";
                mid<<" ";
                down<<subject_seq[s++];
                break;
            }
        }
    }


    for(int c=0; c<scale.str().size();c+=80){
        std::string str=scale.str().substr(c,80);
        std::cout<<str<<" "<<c+str.size()<<std::endl;
        std::cout<<up.str().substr(c,80)<<std::endl;
        std::cout<<mid.str().substr(c,80)<<std::endl;
        std::cout<<down.str().substr(c,80)<<std::endl<<std::endl;
    }
    
    std::cout<<"End PATTERN : "<<end_p<<std::endl;
    std::cout<<"End TEXT : "<<end_t<<std::endl;
    std::cout<<"Length : "<<alen<<std::endl;
    std::cout<<"Identity : "<<identity<<std::endl;
    std::cout<<"Score : "<<score<<std::endl;
}
//------------------------------------------------------------------------------
BioSeq WfaAlign::extractSeq(const BioSeq& sequence, int position, int length, 
            bool is_downstream){

    if(is_downstream){
        return sequence.subseq(position, length);
    }else{
        int len=length;
        if(position-length < 0)
            len=position;
        return sequence.subseq(position-len,len);
    }
}
//------------------------------------------------------------------------------
void WfaAlign::Xdrop(const BioSeq& query_seq, const BioSeq& subject_seq, int xdrop, 
                int qpos, int spos, int len, bool is_downstream, bool is_plusstrand,unsigned verbose)
{
    if(is_plusstrand){
        if(is_downstream){
            BioSeq qsubseq=extractSeq(query_seq,qpos,len);
            BioSeq ssubseq=extractSeq(subject_seq,spos,len);
            XdropAlign(qsubseq, ssubseq,xdrop,verbose);
        }else{
            BioSeq qsubseq=extractSeq(query_seq,qpos,len,false);
            BioSeq qsubseq_rev=qsubseq.reverse();
            BioSeq ssubseq=extractSeq(subject_seq,spos,len,false);
            BioSeq ssubseq_rev=ssubseq.reverse();
            XdropAlign(qsubseq_rev, ssubseq_rev,xdrop,verbose);
        }
    }else{
        if(is_downstream){
            BioSeq qsubseq=extractSeq(query_seq,qpos,len);
            BioSeq subject_seq_comp=subject_seq.complement();
            BioSeq ssubseq=extractSeq(subject_seq_comp,spos,len);
            XdropAlign(qsubseq, ssubseq,xdrop,verbose);
        }else{
            BioSeq qsubseq=extractSeq(query_seq,qpos,len,false);
            BioSeq qsubseq_rev=qsubseq.reverse();
            BioSeq subject_seq_comp=subject_seq.complement();
            BioSeq ssubseq=extractSeq(subject_seq_comp,spos,len,false);
            BioSeq ssubseq_rev=ssubseq.reverse();
            XdropAlign(qsubseq_rev, ssubseq_rev,xdrop,verbose);
        }
    }
}
