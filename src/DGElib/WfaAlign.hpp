//
//  WfaAlign.hpp
//  test_DGE
//
//  Created by Hadi Quesneville on 08/07/2023.
//

#ifndef WfaAlign_hpp
#define WfaAlign_hpp
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstdlib>
#include <cstdio>

#include "BioSeq.h"

#include <wfa2lib/bindings/cpp/WFAligner.hpp>
using namespace wfa;

class WfaAlign : public WFAlignerGapLinear {
    friend class Test_WfaAlign;
    
    int mat, misms, ins, del;
    
    int match;
    int mismatch;
    int gap;
    int end_t;
    int end_p;
    unsigned alen;
    float identity;
    int score;

    void XdropAlign(const BioSeq& query_seq, const BioSeq& subject_seq, int xdrop, unsigned verbose=0);
    BioSeq extractSeq(const BioSeq& sequence, int position, int length,bool is_dowstream=true); 
    
public:
    WfaAlign(const int m=5,
             const int ms=4,
             const int g=8,
             const AlignmentScope alignmentScope=WFAligner::Alignment,
             const MemoryModel memoryModel = MemoryHigh) :
    match(m), mismatch(ms), gap(g),
    WFAlignerGapLinear(m*(-1), ms,g,alignmentScope,memoryModel)
    {
        mat=misms=ins=del=0;
        end_t=0;
        end_p=0;
        alen=0;
        identity=0;
        score=0;
    };
       
    virtual ~WfaAlign(){}
    
    void Xdrop(const BioSeq& query_seq, const BioSeq& subject_seq, int xdrop, 
                int qpos, int spos, int len, bool is_downstream=true, bool is_plusstrand=true,
                unsigned verbose=0);
    void viewAlignment(const BioSeq& query_seq, const BioSeq& subject_seq, unsigned verbose=0);
    float getIdentity(void){return identity;};
    int getScore(void){return score;};
    int getALen(void){return alen;};
    int getEndQ(void){return end_p;};
    int getEndS(void){return end_t;};
};


#endif /* WfaAlign_hpp */
