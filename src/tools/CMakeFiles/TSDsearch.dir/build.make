# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.27

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /Users/hquesnev/homebrew/Cellar/cmake/3.27.7/bin/cmake

# The command to remove a file.
RM = /Users/hquesnev/homebrew/Cellar/cmake/3.27.7/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /Users/hquesnev/src/git/forgemia/te_finder

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /Users/hquesnev/src/git/forgemia/te_finder

# Include any dependencies generated for this target.
include src/tools/CMakeFiles/TSDsearch.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include src/tools/CMakeFiles/TSDsearch.dir/compiler_depend.make

# Include the progress variables for this target.
include src/tools/CMakeFiles/TSDsearch.dir/progress.make

# Include the compile flags for this target's objects.
include src/tools/CMakeFiles/TSDsearch.dir/flags.make

src/tools/CMakeFiles/TSDsearch.dir/TSDsearch.cpp.o: src/tools/CMakeFiles/TSDsearch.dir/flags.make
src/tools/CMakeFiles/TSDsearch.dir/TSDsearch.cpp.o: src/tools/TSDsearch.cpp
src/tools/CMakeFiles/TSDsearch.dir/TSDsearch.cpp.o: src/tools/CMakeFiles/TSDsearch.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green --progress-dir=/Users/hquesnev/src/git/forgemia/te_finder/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object src/tools/CMakeFiles/TSDsearch.dir/TSDsearch.cpp.o"
	cd /Users/hquesnev/src/git/forgemia/te_finder/src/tools && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT src/tools/CMakeFiles/TSDsearch.dir/TSDsearch.cpp.o -MF CMakeFiles/TSDsearch.dir/TSDsearch.cpp.o.d -o CMakeFiles/TSDsearch.dir/TSDsearch.cpp.o -c /Users/hquesnev/src/git/forgemia/te_finder/src/tools/TSDsearch.cpp

src/tools/CMakeFiles/TSDsearch.dir/TSDsearch.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green "Preprocessing CXX source to CMakeFiles/TSDsearch.dir/TSDsearch.cpp.i"
	cd /Users/hquesnev/src/git/forgemia/te_finder/src/tools && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /Users/hquesnev/src/git/forgemia/te_finder/src/tools/TSDsearch.cpp > CMakeFiles/TSDsearch.dir/TSDsearch.cpp.i

src/tools/CMakeFiles/TSDsearch.dir/TSDsearch.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green "Compiling CXX source to assembly CMakeFiles/TSDsearch.dir/TSDsearch.cpp.s"
	cd /Users/hquesnev/src/git/forgemia/te_finder/src/tools && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /Users/hquesnev/src/git/forgemia/te_finder/src/tools/TSDsearch.cpp -o CMakeFiles/TSDsearch.dir/TSDsearch.cpp.s

# Object files for target TSDsearch
TSDsearch_OBJECTS = \
"CMakeFiles/TSDsearch.dir/TSDsearch.cpp.o"

# External object files for target TSDsearch
TSDsearch_EXTERNAL_OBJECTS =

src/tools/TSDsearch: src/tools/CMakeFiles/TSDsearch.dir/TSDsearch.cpp.o
src/tools/TSDsearch: src/tools/CMakeFiles/TSDsearch.dir/build.make
src/tools/TSDsearch: src/SDGlib/libSDGlib.a
src/tools/TSDsearch: src/DGElib/libDGElib.a
src/tools/TSDsearch: src/BLRlib/libBLRlib.a
src/tools/TSDsearch: /usr/local/lib/libwfa2cpp.a
src/tools/TSDsearch: src/tools/CMakeFiles/TSDsearch.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green --bold --progress-dir=/Users/hquesnev/src/git/forgemia/te_finder/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable TSDsearch"
	cd /Users/hquesnev/src/git/forgemia/te_finder/src/tools && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/TSDsearch.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/tools/CMakeFiles/TSDsearch.dir/build: src/tools/TSDsearch
.PHONY : src/tools/CMakeFiles/TSDsearch.dir/build

src/tools/CMakeFiles/TSDsearch.dir/clean:
	cd /Users/hquesnev/src/git/forgemia/te_finder/src/tools && $(CMAKE_COMMAND) -P CMakeFiles/TSDsearch.dir/cmake_clean.cmake
.PHONY : src/tools/CMakeFiles/TSDsearch.dir/clean

src/tools/CMakeFiles/TSDsearch.dir/depend:
	cd /Users/hquesnev/src/git/forgemia/te_finder && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /Users/hquesnev/src/git/forgemia/te_finder /Users/hquesnev/src/git/forgemia/te_finder/src/tools /Users/hquesnev/src/git/forgemia/te_finder /Users/hquesnev/src/git/forgemia/te_finder/src/tools /Users/hquesnev/src/git/forgemia/te_finder/src/tools/CMakeFiles/TSDsearch.dir/DependInfo.cmake "--color=$(COLOR)"
.PHONY : src/tools/CMakeFiles/TSDsearch.dir/depend

