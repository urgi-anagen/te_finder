# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.27

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /Users/hquesnev/homebrew/Cellar/cmake/3.27.7/bin/cmake

# The command to remove a file.
RM = /Users/hquesnev/homebrew/Cellar/cmake/3.27.7/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /Users/hquesnev/src/git/forgemia/te_finder

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /Users/hquesnev/src/git/forgemia/te_finder

# Include any dependencies generated for this target.
include src/tools/CMakeFiles/align2recon.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include src/tools/CMakeFiles/align2recon.dir/compiler_depend.make

# Include the progress variables for this target.
include src/tools/CMakeFiles/align2recon.dir/progress.make

# Include the compile flags for this target's objects.
include src/tools/CMakeFiles/align2recon.dir/flags.make

src/tools/CMakeFiles/align2recon.dir/align2recon.cpp.o: src/tools/CMakeFiles/align2recon.dir/flags.make
src/tools/CMakeFiles/align2recon.dir/align2recon.cpp.o: src/tools/align2recon.cpp
src/tools/CMakeFiles/align2recon.dir/align2recon.cpp.o: src/tools/CMakeFiles/align2recon.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green --progress-dir=/Users/hquesnev/src/git/forgemia/te_finder/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object src/tools/CMakeFiles/align2recon.dir/align2recon.cpp.o"
	cd /Users/hquesnev/src/git/forgemia/te_finder/src/tools && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT src/tools/CMakeFiles/align2recon.dir/align2recon.cpp.o -MF CMakeFiles/align2recon.dir/align2recon.cpp.o.d -o CMakeFiles/align2recon.dir/align2recon.cpp.o -c /Users/hquesnev/src/git/forgemia/te_finder/src/tools/align2recon.cpp

src/tools/CMakeFiles/align2recon.dir/align2recon.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green "Preprocessing CXX source to CMakeFiles/align2recon.dir/align2recon.cpp.i"
	cd /Users/hquesnev/src/git/forgemia/te_finder/src/tools && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /Users/hquesnev/src/git/forgemia/te_finder/src/tools/align2recon.cpp > CMakeFiles/align2recon.dir/align2recon.cpp.i

src/tools/CMakeFiles/align2recon.dir/align2recon.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green "Compiling CXX source to assembly CMakeFiles/align2recon.dir/align2recon.cpp.s"
	cd /Users/hquesnev/src/git/forgemia/te_finder/src/tools && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /Users/hquesnev/src/git/forgemia/te_finder/src/tools/align2recon.cpp -o CMakeFiles/align2recon.dir/align2recon.cpp.s

# Object files for target align2recon
align2recon_OBJECTS = \
"CMakeFiles/align2recon.dir/align2recon.cpp.o"

# External object files for target align2recon
align2recon_EXTERNAL_OBJECTS =

src/tools/align2recon: src/tools/CMakeFiles/align2recon.dir/align2recon.cpp.o
src/tools/align2recon: src/tools/CMakeFiles/align2recon.dir/build.make
src/tools/align2recon: src/SDGlib/libSDGlib.a
src/tools/align2recon: src/DGElib/libDGElib.a
src/tools/align2recon: src/BLRlib/libBLRlib.a
src/tools/align2recon: /usr/local/lib/libwfa2cpp.a
src/tools/align2recon: src/tools/CMakeFiles/align2recon.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color "--switch=$(COLOR)" --green --bold --progress-dir=/Users/hquesnev/src/git/forgemia/te_finder/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable align2recon"
	cd /Users/hquesnev/src/git/forgemia/te_finder/src/tools && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/align2recon.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/tools/CMakeFiles/align2recon.dir/build: src/tools/align2recon
.PHONY : src/tools/CMakeFiles/align2recon.dir/build

src/tools/CMakeFiles/align2recon.dir/clean:
	cd /Users/hquesnev/src/git/forgemia/te_finder/src/tools && $(CMAKE_COMMAND) -P CMakeFiles/align2recon.dir/cmake_clean.cmake
.PHONY : src/tools/CMakeFiles/align2recon.dir/clean

src/tools/CMakeFiles/align2recon.dir/depend:
	cd /Users/hquesnev/src/git/forgemia/te_finder && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /Users/hquesnev/src/git/forgemia/te_finder /Users/hquesnev/src/git/forgemia/te_finder/src/tools /Users/hquesnev/src/git/forgemia/te_finder /Users/hquesnev/src/git/forgemia/te_finder/src/tools /Users/hquesnev/src/git/forgemia/te_finder/src/tools/CMakeFiles/align2recon.dir/DependInfo.cmake "--color=$(COLOR)"
.PHONY : src/tools/CMakeFiles/align2recon.dir/depend

