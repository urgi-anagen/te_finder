# Install script for directory: /Users/hquesnev/src/git/forgemia/te_finder/src/tools

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/objdump")
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/cutterDB")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/cutterDB")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/cutterDB" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/cutterDB")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/cutterDB")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/cutterDB")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/itrsearch")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/itrsearch")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/itrsearch" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/itrsearch")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/itrsearch")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/itrsearch")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/ltrsearch")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/ltrsearch")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/ltrsearch" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/ltrsearch")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/ltrsearch")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/ltrsearch")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/TRsearch")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/TRsearch")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/TRsearch" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/TRsearch")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/TRsearch")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/TRsearch")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/polyAtail")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/polyAtail")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/polyAtail" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/polyAtail")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/polyAtail")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/polyAtail")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/TSDsearch")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/TSDsearch")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/TSDsearch" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/TSDsearch")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/TSDsearch")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/TSDsearch")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/map2db")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/map2db")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2db" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2db")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2db")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2db")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/mapdist")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/mapdist")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapdist" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapdist")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapdist")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapdist")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/mapsize")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/mapsize")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapsize" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapsize")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapsize")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapsize")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank53")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/map2flank53")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank53" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank53")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank53")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank53")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank5")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/map2flank5")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank5" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank5")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank5")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank5")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank3")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/map2flank3")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank3" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank3")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank3")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/map2flank3")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/mapview")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/mapview")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapview" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapview")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapview")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapview")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/mapOp")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/mapOp")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapOp" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapOp")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapOp")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/mapOp")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/refalign")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/refalign")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/refalign" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/refalign")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/refalign")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/refalign")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/statalign")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/statalign")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/statalign" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/statalign")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/statalign")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/statalign")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/fastlalign")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/fastlalign")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/fastlalign" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/fastlalign")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/fastlalign")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/fastlalign")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/hrepeat")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/hrepeat")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/hrepeat" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/hrepeat")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/hrepeat")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/hrepeat")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/halign")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/halign")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/halign" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/halign")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/halign")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/halign")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/hsearch")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/hsearch")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/hsearch" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/hsearch")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/hsearch")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/hsearch")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/SWalign")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/SWalign")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/SWalign" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/SWalign")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/SWalign")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/SWalign")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/NWalign")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/NWalign")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/NWalign" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/NWalign")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/NWalign")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/NWalign")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/orienter")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/orienter")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/orienter" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/orienter")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/orienter")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/orienter")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/setnum2id")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/setnum2id")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/setnum2id" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/setnum2id")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/setnum2id")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/setnum2id")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/pathnum2id")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/pathnum2id")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/pathnum2id" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/pathnum2id")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/pathnum2id")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/pathnum2id")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/align2piler")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/align2piler")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2piler" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2piler")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2piler")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2piler")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/align2recon")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/align2recon")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2recon" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2recon")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2recon")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2recon")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/align2bed")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/align2bed")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2bed" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2bed")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2bed")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/align2bed")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/filterAlign")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/filterAlign")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/filterAlign" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/filterAlign")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/filterAlign")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/filterAlign")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/tabnum2id")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/tabnum2id")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/tabnum2id" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/tabnum2id")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/tabnum2id")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/tabnum2id")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/hquesnev/src/git/forgemia/te_finder/bin/rpt_map")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/Users/hquesnev/src/git/forgemia/te_finder/bin" TYPE EXECUTABLE FILES "/Users/hquesnev/src/git/forgemia/te_finder/src/tools/rpt_map")
  if(EXISTS "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/rpt_map" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/rpt_map")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/SDGlib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/DGElib"
      -delete_rpath "/Users/hquesnev/src/git/forgemia/te_finder/src/BLRlib"
      "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/rpt_map")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" -u -r "$ENV{DESTDIR}/Users/hquesnev/src/git/forgemia/te_finder/bin/rpt_map")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/Users/hquesnev/src/git/forgemia/te_finder/src/tools/testfiles/cmake_install.cmake")

endif()

