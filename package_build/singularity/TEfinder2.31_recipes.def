#distribution based on: debian 9.5
Bootstrap:docker
From:debian:9.5-slim

# container for TE_finder 2.31
# Build:
# singularity build --remote TEfinder2.31.sif TEfinder2.31_recipes.def

%environment
export LC_ALL=C
export LC_NUMERIC=en_GB.UTF-8
export PATH="/opt/miniconda/bin:$PATH"
export PATH="/opt/TE_finder/bin:$PATH"

%labels
AUTHOR Hadi Quesneville
VERSION 2.0
LICENSE GNU
DATE_MODIF 15/11/2021
PACKAGE TE_finder (v2.31)

%help
Container for TE_finder 2.31
Install packages using Miniconda3 V4.7.10
All packages are in /opt/miniconda/bin & are in PATH
Default runscript: no

Usage:
    ./TE_finder.sif  <TE_finder software> -h
    or:
    singularity exec TE_finder.sif <TE_finder software> -h


%runscript
    #passing all arguments from cli: $@
    #!/bin/bash
        if [ $# -eq 0 ]; then
            echo "Usage:"
            echo "  TE_finder.sif  <TE_finder software> -h"
            echo "  or:"
            echo "  singularity exec TE_finder.sif <TE_finder software> -h"
            exit 1
        fi
    exec "$@"

%post

    #essential stuff but minimal
    apt-get update && apt-get install wget -y

    #install conda
    cd /opt
    rm -fr miniconda

    #miniconda3: get miniconda3 version 4.7.10
    wget https://repo.continuum.io/miniconda/Miniconda3-4.7.10-Linux-x86_64.sh -O miniconda.sh

    #install conda
    bash miniconda.sh -b -p /opt/miniconda
    export PATH="/opt/miniconda/bin:$PATH"
    #add channels
    conda config --add channels defaults
    conda config --add channels bioconda
    conda config --add channels conda-forge

    #install blast version 2.9.0
    conda install -c bioconda blast=2.9.0

    #install TE_finder version 2.31
    apt-get install cmake -y
    apt-get install g++ -y
    apt-get install libcppunit-dev -y
    apt-get install git -y
    cd /opt
    mkdir TE_finder
    cd TE_finder
    git clone https://github.com/urgi-anagen/TE_finder.git
    cd TE_finder
    cmake .
    make install
    cd ..
    mkdir bin
    cp TE_finder/bin/* bin
    rm -rf TE_finder

    #cleanup
    conda clean -y --all
    rm -f /opt/miniconda.sh
    apt-get autoremove --purge
    apt-get clean
