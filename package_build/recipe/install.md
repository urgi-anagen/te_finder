# Description

This directory (the recipe directory) contains all the files that you need to build a working TE_finder conda packages

# Dependencies

Have conda or anaconda3 installed

Have the conda-forge and bioconda channels setup as default channels. if it is not the case, use the following command

    conda config --add channels conda-forge
    conda config --add channels bioconda

# Known issue

If you have an already installed version of libcppunit, the package build will fail, uninstall libcppunit, build the package, and then reinstall it later

# Build instruction

Use the "Conda build" command on the "recipe" directory

    conda build recipe

The local package thus created will be named "te_finder"

Wait until the build is finished, then install the local package that you created on one of your conda environnement or create a brand new one for te_finder:

    conda install -c local "te_finder"

or

    conda create -n TE_finder -c local "te_finder"
